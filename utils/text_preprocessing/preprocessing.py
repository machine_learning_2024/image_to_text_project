import re
import nltk
# nltk.download('stopwords')
# from nltk.corpus import stopwords
from keras_preprocessing.text import Tokenizer


class TextPreprocessor:
    def __init__(self):
        self.contractions_dict = {
            "won't": "will not", "can't": "cannot", "shouldn't": "should not", "wouldn't": "would not",
            "don't": "do not", "doesn't": "does not", "didn't": "did not", "haven't": "have not",
            "hasn't": "has not", "hadn't": "had not", "isn't": "is not", "aren't": "are not",
            "wasn't": "was not", "weren't": "were not", "couldn't": "could not", "mustn't": "must not",
            "mightn't": "might not", "shan't": "shall not", "it's": "it is", "I'm": "I am",
            "we're": "we are", "they're": "they are", "you're": "you are", "that's": "that is",
            "who's": "who is", "what's": "what is", "where's": "where is", "when's": "when is",
            "why's": "why is", "how's": "how is", "it'll": "it will", "he'll": "he will",
            "she'll": "she will", "we'll": "we will", "they'll": "they will", "you'll": "you will",
            "I'll": "I will", "I've": "I have", "you've": "you have", "we've": "we have",
            "they've": "they have", "could've": "could have", "would've": "would have",
            "should've": "should have", "might've": "might have", "must've": "must have",
        }
        self.punctuation_pattern = re.compile('[^\w\s]')

    def lowercase_text(self, text_original):
        return text_original.lower()

    def expand_contractions(self, text):
        return ' '.join([self.contractions_dict.get(word, word) for word in text.split()])

    def remove_punctuation(self, text_original):
        return self.punctuation_pattern.sub('', text_original)

    def remove_single_character(self, text):
        return ' '.join(word for word in text.split() if len(word) > 1)

    def remove_numeric(self, text):
        return ' '.join(word for word in text.split() if word.isalpha())

    def add_start_end_seq_token(self, text):
        return f'startseq {text} endseq'

    def preprocess_caption(self, caption):
        processed = self.lowercase_text(caption)
        processed = self.expand_contractions(processed)
        processed = self.remove_punctuation(processed)
        processed = self.remove_single_character(processed)
        processed = self.remove_numeric(processed)
        return self.add_start_end_seq_token(processed)


class WordsTokenizer:
    def __init__(self, captions):
        self.captions = captions
        self.tokenizer = Tokenizer()
        self.tokenize()

    def tokenize(self):
        self.tokenizer.fit_on_texts(self.captions)
        self.vocab_size = len(self.tokenizer.word_index) + 1
        self.tokenized_texts = self.tokenizer.texts_to_sequences(self.captions)
