from nltk.translate.bleu_score import sentence_bleu

from utils.inference.inference import generate_caption

START_TOKEN = 'startseq'
END_TOKEN = 'endseq'


def process_caption(tokenized_caption, index_word):
    return [index_word[token] for token in tokenized_caption if token not in (START_TOKEN, END_TOKEN)]


def predict_and_evaluate(model, tokenizer, img_features, true_caption, index_word, max_cap):
    predicted_caption = generate_caption(model, img_features.reshape(1, -1), tokenizer, index_word,
                                         sequence_limit=max_cap).split()[1:-1]
    bleu_score = sentence_bleu([true_caption], predicted_caption, weights=(0.25, 0.25, 0.25))
    return bleu_score, predicted_caption


def process_images(filenames, image_features, tokenized_texts, model, tokenizer, index_word, max_cap):
    bleu_scores = []

    for count, (filename, img_feature, tokenized_text) in enumerate(zip(filenames, image_features, tokenized_texts), 1):
        true_caption = process_caption(tokenized_text, index_word)
        bleu_score, predicted_caption = predict_and_evaluate(model, tokenizer, img_feature, true_caption, index_word,
                                                             max_cap)
        bleu_scores.append(bleu_score)

    return bleu_scores
