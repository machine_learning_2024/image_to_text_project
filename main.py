import pickle

import numpy as np
from flask import Flask, render_template, request
from keras.models import load_model
from utils.features_images.features_extractor import FeatureExtractor
from utils.inference.inference import generate_caption
from nltk.translate.bleu_score import sentence_bleu
from nltk.tokenize import word_tokenize

app = Flask(__name__)

model_paths = {
    "densenet_lstm": "model_demos/models_weights/densenet_lstm.keras",
    "resnet_lstm": "model_demos/models_weights/resnet_lstm.keras",
    "vgg_lstm": "model_demos/models_weights/vgg_lstm.keras",
    "inception_lstm": "model_demos/models_weights/inception_lstm.keras",
    "efficient_lstm": "model_demos/models_weights/efficientnet_lstm.keras"
}

feature_extractor = {
    "densenet_lstm": FeatureExtractor('densenet121'),
    "resnet_lstm": FeatureExtractor('resnet50'),
    "vgg_lstm": FeatureExtractor('vgg16'),
    "inception_lstm": FeatureExtractor('inceptionv3'),
    "efficient_lstm": FeatureExtractor('efficientnet')
}

with open('model_demos/tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)
index_word = dict([(index, word) for word, index in tokenizer.tokenizer.word_index.items()])
max_cap = np.max([len(cap) for cap in tokenizer.tokenized_texts])


@app.route('/')
def main():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def predict():
    if request.method == "POST":
        f = request.files['userfile']
        path = "./static/{}".format(f.filename)
        f.save(path)

        model_choice = request.form['model_choice']
        model_path = model_paths.get(model_choice)
        model = load_model(model_path)

        extractor = feature_extractor.get(model_choice)

        image_feature = extractor.extract_features([f.filename], "static/")
        image_feature = image_feature[f.filename].reshape(1, -1)

        gen_caption = generate_caption(model, [image_feature], tokenizer, index_word, sequence_limit=max_cap)[9:-7]

        results_dic = {
            'image': "static/{}".format(f.filename),
            'caption': gen_caption
        }

    return render_template("index.html", result=results_dic)


@app.route('/compare_caption', methods=['POST'])
def compare_caption():
    user_input = request.form['user_caption']
    original_caption = request.form['original_caption']
    evaluation_type = request.form['evaluation_type']

    reference = word_tokenize(original_caption.lower())
    candidate = word_tokenize(user_input.lower())
    score = 0

    if evaluation_type == 'BLEU':
        score = sentence_bleu([reference], candidate)

    return render_template("index.html", result={'image': request.form['image_path'], 'caption': original_caption},
                           bleu_score=score)


if __name__ == '__main__':
    app.run(debug=True)
