function resetPage() {
    document.querySelector('form').reset();
    document.getElementById('resetButton').style.display = 'none';
    document.getElementById('submitButton').disabled = true;
}

function enableSubmitButton() {
    const fileInput = document.querySelector('input[type="file"]').files.length;
    const modelSelected = document.querySelector('select').value.trim() !== "";
    document.getElementById('submitButton').disabled = !(fileInput && modelSelected);
}

function toggleResetButton() {
    document.getElementById('resetButton').style.display = 'block';
}

function speakText(text) {
    const synth = window.speechSynthesis;
    const utterance = new SpeechSynthesisUtterance(text);
    synth.speak(utterance);
}
