from keras import layers
from keras import models

class Encoder:
    def __init__(self, input_shape, dropout_rate=0):
        self.input_shape = input_shape
        self.dropout_rate = dropout_rate

    def build(self):
        input_image = layers.Input(shape=(self.input_shape,))
        l1 = layers.Dropout(self.dropout_rate)(input_image)
        l2 = layers.Dense(256, activation='relu')(l1)
        return input_image, l2

class Decoder:
    def __init__(self, vocab_size, max_cap, dropout_rate=0, bidirectional=False):
        self.vocab_size = vocab_size
        self.max_cap = max_cap
        self.dropout_rate = dropout_rate
        self.bidirectional = bidirectional

    def build(self):
        dim_embedding = 64

        input_txt = layers.Input(shape=(self.max_cap,))
        l1 = layers.Embedding(self.vocab_size, dim_embedding, mask_zero=True)(input_txt)
        l2 = layers.Dropout(self.dropout_rate)(l1)

        if self.bidirectional:
            l3 = layers.Bidirectional(layers.LSTM(128))(l2)
        else:
            l3 = layers.LSTM(256)(l2)

        return input_txt, l3

class CaptioningModel:
    def __init__(self, encoder, decoder,
                 loss='categorical_crossentropy',
                 optimizer='adam',
                 activation_decoder='relu',
                 activation_output='softmax'):

        self.encoder = encoder
        self.decoder = decoder
        self.loss = loss
        self.optimizer = optimizer
        self.activation_decoder = activation_decoder
        self.activation_output = activation_output

    def build_model(self):
        input_image, fimage = self.encoder.build()
        input_txt, ftxt = self.decoder.build()

        decoder = layers.add([ftxt, fimage])
        decoder = layers.Dense(256, activation=self.activation_decoder)(decoder)
        output = layers.Dense(self.decoder.vocab_size, activation=self.activation_output)(decoder)

        model = models.Model(inputs=[input_image, input_txt], outputs=output)

        model.compile(loss=self.loss, optimizer=self.optimizer)

        return model
