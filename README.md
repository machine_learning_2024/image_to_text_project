# Image-To-Text: Experiments and Application

## Aim

The aim of the project is to experiment with various models, evaluate their performance using the BLEU score, and develop a user-friendly web application that converts images into descriptive text and audio for visually impaired users.

## Data 

The dataset is not provided in this repository. Please [download](https://huggingface.co/datasets/atasoglu/flickr8k-dataset) it manually.

## Experimental Component

This part of the project focuses on trying out different types of models to see which one works best for converting images to text. Experiments involve trying several well-known models like VGG, DenseNet, EfficientNet, ResNet and Inception in combination with LSTM and Bidirectional LSTM, which are good at understanding and generating text.

For further information, refer to: https://arxiv.org/abs/1411.4555

The models are ranked according to their BLEU scores as follows:

1. _Resnet + Bidirectional LSTM_

2. _Resnet + LSTM_

3. _Inception + Bidirectional LSTM_

4. _Inception + LSTM_

5. _VGG + LSTM_

6. _Densenet + Bidirectional LSTM_

7. _Efficientnet + Bidirectional LSTM_

8. _VGG + Bidirectional LSTM_

9. _Efficientnet + LSTM_

## Application

The application developed from this project will be simple to use. A user will upload an image to the website, and the system will generate a caption that describes the image. This caption will then be read aloud through the website's audio feature, making it easier for visually impaired users to understand what the image shows without seeing it.

Here are some examples from the application:

![cycle_demo.png](media%2Fcycle_demo.png)

![dog_demo.png](media%2Fdog_demo.png)
