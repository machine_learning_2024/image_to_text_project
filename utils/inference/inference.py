import numpy as np
from keras_preprocessing.sequence import pad_sequences
from matplotlib import pyplot as plt
from PIL import Image


def generate_caption(model, img_data, tokenizer, word_index_map, init_word='startseq', final_word='endseq',
                     sequence_limit=100):
    generated_text = init_word

    for i in range(sequence_limit):
        sequence = tokenizer.tokenizer.texts_to_sequences([generated_text])[0]
        sequence = pad_sequences([sequence], maxlen=sequence_limit)

        output_probabilities = model.predict([img_data, sequence], verbose=0)
        next_word_id = np.argmax(output_probabilities)
        next_word = word_index_map[next_word_id]

        generated_text += ' ' + next_word

        if next_word == final_word:
            break

    return generated_text


def display_image_captions(from_index, to_index, file_list, encoded_feats, img_dir, model, max_length, tokenizer,
                           word_index_map):
    plt.figure(figsize=(10, 5 * (to_index - from_index)))
    for idx, (file_name, encoded_feat) in enumerate(
            zip(file_list[from_index:to_index], encoded_feats[from_index:to_index]), start=1):
        image_path = f'{img_dir}/{file_name}'
        image = Image.open(image_path)

        reshaped_feature = encoded_feat.reshape(1, -1)
        caption = generate_caption(model, reshaped_feature, tokenizer, word_index_map, sequence_limit=max_length)

        plt.subplot(to_index - from_index, 1, idx)
        plt.imshow(image)
        plt.title(caption)
        plt.axis('off')
    plt.tight_layout()
    plt.show()
