import numpy as np
import pandas as pd

from utils.modeling.encoder_decoder import Encoder, Decoder, CaptioningModel


def read_file_and_process(captions_path):
    def process_line(line):
        col = line.split('\t')
        if len(col) > 1:
            w = col[0].split("#")
            return w + [col[1].lower()]

    with open(captions_path, 'r') as file:
        lines = file.readlines()

    datatxt = [process_line(line) for line in lines]
    datatxt = [line for line in datatxt if line]

    return pd.DataFrame(datatxt, columns=["filename", "index", "caption"])


def form_data(df, imgs):
    dimages, keepindex = [], []

    # if caption_idx:
    subset_df = df.loc[df["index"].values == "0", :]
    # else:
    #     subset_df = df.loc[:, :]

    for index, filename in enumerate(subset_df.filename):
        if filename in imgs.keys():
            dimages.append(imgs[filename])
            keepindex.append(index)

    selected_filenames = subset_df["filename"].iloc[keepindex].values
    selected_captions = subset_df["caption"].iloc[keepindex].values
    selected_images = np.array(dimages)

    return selected_filenames, selected_captions, selected_images


def init_model(train_data, tokenizer, max_cap, bidirectional=False):
    encoder = Encoder(input_shape=train_data.shape[1])
    decoder = Decoder(vocab_size=tokenizer.vocab_size, max_cap=max_cap, bidirectional=bidirectional)

    caption_model = CaptioningModel(encoder, decoder)

    model = caption_model.build_model()

    return model