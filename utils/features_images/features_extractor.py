from keras.preprocessing.image import load_img, img_to_array
from keras.applications import InceptionV3, VGG16, ResNet50, DenseNet121, Xception, EfficientNetB0
from keras.applications.inception_v3 import preprocess_input as preprocess_input_inception
from keras.applications.vgg16 import preprocess_input as preprocess_input_vgg
from keras.applications.resnet import preprocess_input as preprocess_input_resnet
from keras.applications.densenet import preprocess_input as preprocess_input_densenet
from keras.applications.xception import preprocess_input as preprocess_input_xception
from keras.applications.efficientnet import preprocess_input as preprocess_input_efficientnet
from collections import OrderedDict
from keras import models

class FeatureExtractor:
    def __init__(self, model_name):
        if model_name.lower() == 'inceptionv3':
            self.model = InceptionV3(include_top=True, weights=None)
            self.model.load_weights("inception_v3_weights_tf_dim_ordering_tf_kernels.h5")
            self.preprocess_fn = preprocess_input_inception
            self.npix = 299
        elif model_name.lower() == 'vgg16':
            self.model = VGG16(include_top=True, weights=None)
            self.model.load_weights("vgg16_weights_tf_dim_ordering_tf_kernels.h5")
            self.preprocess_fn = preprocess_input_vgg
            self.npix = 224
        elif model_name.lower() == 'resnet50':
            self.model = ResNet50(include_top=False, weights='imagenet', pooling='avg')
            self.preprocess_fn = preprocess_input_resnet
            self.npix = 224
        elif model_name.lower() == 'densenet121':
            self.model = DenseNet121(include_top=True, weights=None)
            self.model.load_weights("DenseNet-BC-121-32.h5")
            self.preprocess_fn = preprocess_input_densenet
            self.npix = 224
        elif model_name.lower() == 'xception':
            self.model = Xception(include_top=False, weights=None)
            self.model.load_weights("xception_weights_tf_dim_ordering_tf_kernels_notop.h5")
            self.preprocess_fn = preprocess_input_xception
            self.npix = 200
        elif model_name.lower() == 'efficientnet':
            self.model = EfficientNetB0(include_top=False, weights='imagenet')
            self.preprocess_fn = preprocess_input_efficientnet
            self.npix = 224
        else:
            raise ValueError("Unsupported model.")

        self.model.layers.pop()
        self.model = models.Model(inputs=self.model.inputs, outputs=self.model.layers[-1].output)

    def extract_features(self, jpgs, images_path):
        images = {}
        target_size = (self.npix, self.npix, 3)

        for name in jpgs:
            filename = f"{images_path}/{name}"
            image = load_img(filename, target_size=target_size)
            image = img_to_array(image)
            nimage = self.preprocess_fn(image)
            y_pred = self.model.predict(nimage.reshape((1,) + nimage.shape[:3]))
            images[name] = y_pred.flatten()

        return images


