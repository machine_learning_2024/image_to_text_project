from keras_preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
import numpy as np


class DataPreprocessor:
    def __init__(self, test_ratio=0.2, validation_ratio=0.2):
        self.test_ratio = test_ratio
        self.validation_ratio = validation_ratio

    def split_test_val_train(self, sequences, count_test, count_val):
        return (
            sequences[:count_test], sequences[count_test:count_test + count_val], sequences[count_test + count_val:])

    def preprocessing(self, captions, images, max_length, tokenizer):
        total = len(captions)
        print("# captions/images = {}".format(total))

        input_text, input_image, output_text = [], [], []
        for caption, image in zip(captions, images):
            for idx in range(1, len(caption)):
                seq_in, seq_out = caption[:idx], caption[idx]
                seq_in = pad_sequences([seq_in], maxlen=max_length).flatten()
                seq_out = to_categorical(seq_out, num_classes=tokenizer.vocab_size)

                input_text.append(seq_in)
                input_image.append(image)
                output_text.append(seq_out)

        return np.array(input_text), np.array(input_image), np.array(output_text)


def prepare_data(self, custom_tokenizer, text, image_data, filenames, max_length):
    total_texts = len(custom_tokenizer.tokenized_texts)
    num_test, num_val = int(total_texts * self.test_ratio), int(total_texts * self.validation_ratio)

    text_test, text_val, text_train = self.partition_data(custom_tokenizer.tokenized_texts, num_test, num_val)
    image_test, image_val, image_train = self.partition_data(image_data, num_test, num_val)
    file_test, file_val, file_train = self.partition_data(filenames, num_test, num_val)

    train_texts, train_images, train_labels = self.process_captions(text_train, image_train, max_length,
                                                                    custom_tokenizer)
    val_texts, val_images, val_labels = self.process_captions(text_val, image_val, max_length, custom_tokenizer)

    return (train_texts, train_images, train_labels, val_texts, val_images, val_labels, file_test, file_val, file_train,
            text_test, image_test)
